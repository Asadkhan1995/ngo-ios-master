

import UIKit
import Lottie
import CoreLocation

class SOSViewController: UIViewController,CLLocationManagerDelegate
 {
    //@IBOutlet weak var animationView: LottieView!
    let animationView = AnimationView()
    @IBOutlet weak var locationlbl: UILabel!
    var locationManager:CLLocationManager!
    override func viewDidLoad() {
        super.viewDidLoad()
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        
        if CLLocationManager.locationServicesEnabled(){
            locationManager.startUpdatingLocation()
        }
    }
    func startanimation()
    {
        animationView.animation = Animation.named("animscan")
        animationView.frame = view.bounds
        animationView.backgroundColor = .white
        animationView.contentMode = .scaleAspectFit
        animationView.loopMode = .loop
        animationView.play()
        view.addSubview(animationView)
        
    
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation :CLLocation = locations[0] as CLLocation
        
        print("user latitude = \(userLocation.coordinate.latitude)")
        print("user longitude = \(userLocation.coordinate.longitude)")
        getAddressFromLatLon(pdblLatitude: userLocation.coordinate.latitude, withLongitude : userLocation.coordinate.longitude)
    }
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error \(error)")
    }

    func getAddressFromLatLon(pdblLatitude: Double, withLongitude : Double) {
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        let lat: Double = pdblLatitude
        //21.228124
        let lon: Double = withLongitude
        //72.833770
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = lat
        center.longitude = lon
        
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
        
        
        ceo.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                if (error != nil)
                {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                }
                guard let place = placemarks else
                {
                    print("cannot get Location")
                    return
                    
                }
                let pm = place
                
                if pm.count > 0 {
                    let pm = placemarks![0]
                    //                    print(pm.country)
                    //                    print(pm.locality)
                    //                    print(pm.subLocality)
                    //                    print(pm.thoroughfare)
                    //                    print(pm.postalCode)
                    //                    print(pm.subThoroughfare)
                    var addressString : String = ""
                    if pm.subLocality != nil {
                        addressString = addressString + pm.subLocality! + ","
                    }
                    if pm.thoroughfare != nil {
                        addressString = addressString + pm.thoroughfare! + ","
                    }
                    if pm.locality != nil {
                        addressString = addressString + pm.locality! + ","
                    }
                    if pm.country != nil {
                        addressString = addressString + pm.country! + ", "
                    }
                    if pm.postalCode != nil {
                        addressString = addressString + pm.postalCode! + " "
                    }
                    if addressString != ""{
                        
                        self.locationlbl.text = addressString
                    }
                    print(addressString)
                }
        })
        
    }

}

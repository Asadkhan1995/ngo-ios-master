//
//  kiddnapTableViewCell.swift
//  NGO
//
//  Created by Obaid Khan on 04/03/2020.
//  Copyright © 2020 Obaid Khan. All rights reserved.
//

import UIKit

class kiddnapTableViewCell: UITableViewCell {

    @IBOutlet weak var catView: UIView!
    @IBOutlet weak var buttonReportAdmin: UIButton!
    @IBOutlet weak var phoneNo: UILabel!
    @IBOutlet weak var discription: UILabel!
    @IBOutlet weak var location: UILabel!
    @IBOutlet weak var gender: UILabel!
    @IBOutlet weak var age: UILabel!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var imageview: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

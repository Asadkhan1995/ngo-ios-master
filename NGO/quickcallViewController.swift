//
//  quickcallViewController.swift
//  NGO
//
//  Created by Apple on 10/03/2020.
//  Copyright © 2020 Obaid Khan. All rights reserved.
//

import UIKit
import SVProgressHUD
import Alamofire


class quickcallViewController:UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    fileprivate let application = UIApplication.shared
    @IBOutlet weak var quickcollectionview: UICollectionView!
    var data = [quickdata]()
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data.count
    }
   
      func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
         return CGSize(width:(quickcollectionview.bounds.width/2) - 16 , height: 150  )
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cel = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! quickcallCollectionViewCell
        cel.quickbuton.setTitle(data[indexPath.row].name, for: .normal)
        cel.quickbuton.tag = indexPath.row
        let imageName = self.data[indexPath.item].image ?? ""
        
        
        let imageurl = URL(string: "https://enscyd.com/ngo_api/images/"+imageName)!
        if imageurl != nil
        {
           print(imageurl)
           let dataa = try? Data(contentsOf: imageurl)
            if let imageData = dataa {
                let x = UIImage(data: imageData)
               cel.quickimg.image = x
               
            }
            else
            {

               print(Error.self)
            }
        }
        return cel
    }
    
   
    @IBAction func btncall(_ sender: UIButton) {
        let num = data[sender.tag].phone
        print(num)
        if let phoneURl = URL(string: "tel:/"+((num ?? nil)!)){
            print(phoneURl)
            if application.canOpenURL(phoneURl){
                application.open(phoneURl, options: [:], completionHandler: nil)
            }
            else{
                
            }
            
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getData()
        let layout = UICollectionViewFlowLayout()
         layout.scrollDirection = .vertical //.horizontal
        // layout.itemSize = cellSize
         layout.sectionInset = UIEdgeInsets(top: 8, left: 10, bottom: 8, right: 10)
         
         layout.minimumLineSpacing = 8
         layout.minimumInteritemSpacing = 0
        quickcollectionview.setCollectionViewLayout(layout, animated: true)
    }
    
    
    func getData(){
         let url = "https://enscyd.com/ngo_api/showquickCall.php"
        SVProgressHUD.setDefaultStyle(SVProgressHUDStyle.dark)
        SVProgressHUD.show(withStatus: "Loading")
         var record = [NSDictionary]()
         Alamofire.request(url, method: .post, parameters: nil, encoding: JSONEncoding.default).responseJSON
             {responce in
                 switch responce.result{
                 case .success:
                     if let json = responce.result.value{
                         let JSON = json as! NSDictionary
                         
                         for res in JSON{
                             if ("\(res.key)" == "records")
                             {
                                 record = JSON.object(forKey: res.key as! String) as! [NSDictionary]
                                 for innerResult in record{
                                  
                                     let dicFrmArray = innerResult
                                     
                                     if ((dicFrmArray["id"] as? NSNull) == nil && dicFrmArray["name"] != nil && dicFrmArray["phone"] != nil && dicFrmArray["image"] != nil && dicFrmArray["city"] != nil && dicFrmArray["country"] != nil  ){
                                         
                                      
                                         let datas = quickdata(id: (dicFrmArray["id"]) as! String, name: (dicFrmArray["name"]) as! String, phone: (dicFrmArray["phone"]) as! String, image: (dicFrmArray["image"]) as! String, city: (dicFrmArray["city"]) as! String, country: (dicFrmArray["country"]) as! String)
                                        
                                          self.data.append(datas)
                                         print(datas.image)
                                        
                                     }
                                     
                                 }
                                
                             }
                         }
                         self.quickcollectionview.reloadData()
                          SVProgressHUD.dismiss()
                     }
                     else{
                         print("Failure")
                     }
                     break
                 case .failure:
                     if let json = responce.result.value{
                         print(json)
                     }
                     break
                 }
         }
         
     }
    
    

}
struct quickdata{

    let id : String?
    let name : String?
    let phone : String?
    let image : String?
    let city : String?
    let country : String?
}

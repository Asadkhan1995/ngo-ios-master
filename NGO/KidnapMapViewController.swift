//
//  KidnapMapViewController.swift
//  NGO
//
//  Created by Obaid Khan on 06/04/2020.
//  Copyright © 2020 Obaid Khan. All rights reserved.
//

import UIKit
import SVProgressHUD
import Alamofire
class KidnapMapViewController:UIViewController ,UITableViewDataSource,UITableViewDelegate{
  
    
    
    @IBOutlet weak var saveGardiain: UIButton!
       @IBOutlet weak var GuaidainTableView: UITableView!
       var data = [dataa]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
           return data.count
     }
     
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
          let cel = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! kiddnapTableViewCell
        
        return cel
     }
    
    func getData(){
        let url = "https://enscyd.com/ngo_api/showkidnapPeople.php"
       SVProgressHUD.setDefaultStyle(SVProgressHUDStyle.dark)
       SVProgressHUD.show(withStatus: "Loading")
        var record = [NSDictionary]()
        Alamofire.request(url, method: .post, parameters: nil, encoding: JSONEncoding.default).responseJSON
            {responce in
                switch responce.result{
                case .success:
                    if let json = responce.result.value{
                        let JSON = json as! NSDictionary
                        
                        for res in JSON{
                            if ("\(res.key)" == "records")
                            {
                                record = JSON.object(forKey: res.key as! String) as! [NSDictionary]
                                for innerResult in record{
                                 
                                    let dicFrmArray = innerResult
                                    
                                    if ((dicFrmArray["id"] as? NSNull) == nil && dicFrmArray["name"] != nil && dicFrmArray["image"] != nil && dicFrmArray["phone"] != nil && dicFrmArray["address"] != nil && dicFrmArray["created_date"] != nil && dicFrmArray["description"] != nil && dicFrmArray["gender"] != nil && dicFrmArray["age"] != nil ){
                                        
                                     
                                        let datas = dataa(id: (dicFrmArray["id"]) as! String, name: (dicFrmArray["name"]) as! String, image: (dicFrmArray["image"]) as! String, phone: (dicFrmArray["phone"]) as! String, address: (dicFrmArray["address"]) as! String, date: (dicFrmArray["created_date"]) as! String, discription: (dicFrmArray["description"]) as! String, gender: (dicFrmArray["gender"]) as! String, age: (dicFrmArray["age"]) as! String)
                                       
                                         self.data.append(datas)
                                        print(datas.image)
                                       
                                    }
                                    
                                }
                               
                            }
                        }
                        self.GuaidainTableView.reloadData()
                         SVProgressHUD.dismiss()
                    }
                    else{
                        print("Failure")
                    }
                    break
                case .failure:
                    if let json = responce.result.value{
                        print(json)
                    }
                    break
                }
        }
        
    }
   

}


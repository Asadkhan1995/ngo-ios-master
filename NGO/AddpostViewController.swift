//
//  AddpostViewController.swift
//  NGO
//
//  Created by Obaid Khan on 26/02/2020.
//  Copyright © 2020 Obaid Khan. All rights reserved.
//

import UIKit

class AddpostViewController: UIViewController {

    @IBOutlet weak var userImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        userImage.layer.cornerRadius = userImage.frame.height/2
        userImage.clipsToBounds = true
        
        userImage.layer.borderWidth = 2
      userImage.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    }
    
}

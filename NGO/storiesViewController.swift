

import UIKit

class storiesViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cel = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! storiesTableViewCell
        cel.userimage.layer.cornerRadius = cel.userimage.frame.height/2
        cel.userimage.clipsToBounds = true
        cel.userimage.layer.borderWidth = 1
        cel.userimage.layer.borderColor =
           #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        
        
        
        
        return cel
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

   
}

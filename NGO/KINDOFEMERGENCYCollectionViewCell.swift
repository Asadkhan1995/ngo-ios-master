//
//  KINDOFEMERGENCYCollectionViewCell.swift
//  NGO
//
//  Created by Obaid Khan on 02/03/2020.
//  Copyright © 2020 Obaid Khan. All rights reserved.
//

import UIKit

class KINDOFEMERGENCYCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var titlelbl: UILabel!
   
    @IBOutlet weak var checkimg: UIImageView!
    @IBOutlet weak var titleimg: UIImageView!
    @IBOutlet weak var viewimg: UIView!
}

//
//  mainpageViewController.swift
//  NGO
//
//  Created by Apple on 06/03/2020.
//  Copyright © 2020 Obaid Khan. All rights reserved.
//

import UIKit

class mainpageViewController: UIViewController {

    @IBOutlet weak var reportCrime: UIButton!
    @IBOutlet weak var quickcall: UIButton!
    @IBOutlet weak var support: UIButton!
    @IBOutlet weak var law: UIButton!
    @IBOutlet weak var doctor: UIButton!
    @IBOutlet weak var kindap: UIButton!
    @IBOutlet weak var community: UIButton!
    @IBOutlet weak var emergency: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        quickcall.layer.shadowOpacity = 3
        quickcall.layer.shadowColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        quickcall.layer.borderWidth = 1
        quickcall.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        quickcall.layer.cornerRadius = 4
        
        reportCrime.layer.shadowOpacity = 3
        reportCrime.layer.shadowColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        reportCrime.layer.borderWidth = 1
        reportCrime.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        reportCrime.layer.cornerRadius = 4
        
        support.layer.shadowOpacity = 3
        support.layer.shadowColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        support.layer.borderWidth = 1
        support.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        support.layer.cornerRadius = 4
        
        law.clipsToBounds = true
        law.layer.shadowOpacity = 3
        law.layer.shadowColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        law.layer.borderWidth = 1
        law.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        law.layer.cornerRadius = 4
        law.clipsToBounds = true
        
        doctor.layer.shadowOpacity = 3
        doctor.layer.shadowColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        doctor.layer.borderWidth = 1
        doctor.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        doctor.layer.cornerRadius = 4
        doctor.clipsToBounds = true
        
        kindap.layer.shadowOpacity = 3
        kindap.layer.shadowColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        kindap.layer.borderWidth = 1
        kindap.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        kindap.layer.cornerRadius = 4
        kindap.clipsToBounds = true
        
        community.layer.shadowOpacity = 3
        community.layer.shadowColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        community.layer.borderWidth = 1
        community.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        community.layer.cornerRadius = 4
        community.clipsToBounds = true
        
        emergency.layer.shadowOpacity = 3
        emergency.layer.shadowColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        emergency.layer.borderWidth = 1
        emergency.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        emergency.layer.cornerRadius = 4
        emergency.clipsToBounds = true
    }
    
    @IBAction func emergencybtn(_ sender: UIButton) {
        let c = self.storyboard?.instantiateViewController(withIdentifier: "mapViewController") as! mapViewController
        self.navigationController?.pushViewController(c, animated: true)
    }
    
    @IBAction func coomunity(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "storiesViewController") as! storiesViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func kidnapbtn(_ sender: UIButton) {
        let c = self.storyboard?.instantiateViewController(withIdentifier: "kiddnapViewController") as! kiddnapViewController
        self.navigationController?.pushViewController(c, animated: true)
    }
    
    @IBAction func lawbtn(_ sender: Any) {
    let c = self.storyboard?.instantiateViewController(withIdentifier: "mapViewController") as! mapViewController
    self.navigationController?.pushViewController(c, animated: true)
    }
    @IBAction func doctorbtn(_ sender: UIButton) {
    let c = self.storyboard?.instantiateViewController(withIdentifier: "AppointmentViewController") as! AppointmentViewController
    self.navigationController?.pushViewController(c, animated: true)
    }
    
    @IBAction func suppotbtn(_ sender: UIButton) {
      let c = self.storyboard?.instantiateViewController(withIdentifier: "mapViewController") as! mapViewController
    self.navigationController?.pushViewController(c, animated: true)
    }
    
    @IBAction func quickcallbtn(_ sender: UIButton) {
        
        let c = self.storyboard?.instantiateViewController(withIdentifier: "quickcallViewController") as! quickcallViewController
        self.navigationController?.pushViewController(c, animated: true)
    }
    
    @IBAction func crimerepotbtn(_ sender: UIButton) {
        
        let c = self.storyboard?.instantiateViewController(withIdentifier: "mapViewController") as! mapViewController
        self.navigationController?.pushViewController(c, animated: true)
    }
}

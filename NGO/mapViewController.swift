//
//  mapViewController.swift
//  NGO
//
//  Created by Obaid Khan on 03/03/2020.
//  Copyright © 2020 Obaid Khan. All rights reserved.
//

import UIKit
import  Alamofire
import  SVProgressHUD
import CoreLocation

class mapViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,CLLocationManagerDelegate {
    var data = [Edataa]()
    var selectcell = -1
    var locationManager:CLLocationManager!
     var collectionViewArray = [SendtoCollectionViewCell]()
    
    
    @IBOutlet weak var senttoview: UIView!
    @IBOutlet weak var emergencyview: UIView!
    @IBOutlet weak var locationtext: UITextField!
    @IBOutlet weak var mylocationbtn: UIButton!
    @IBOutlet weak var sendtocollection: UICollectionView!
    @IBOutlet weak var emergencycollection: UICollectionView!
    
    
    
   
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.emergencycollection{
        return data.count
        }
        else{
            
            return 5
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == self.emergencycollection {
            let cel = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! KINDOFEMERGENCYCollectionViewCell
            cel.checkimg.isHidden = true
            
            if self.selectcell == indexPath.item
            {
                cel.checkimg.isHidden = false
            }
            
            cel.titlelbl.text = data[indexPath.item].name
             let imageName = self.data[indexPath.item].image ?? ""
            let imageurl = URL(string: "https://enscyd.com/ngo_api/images/"+imageName)!
            if imageurl != nil
              {
                 print(imageurl)
                 let dataa = try? Data(contentsOf: imageurl)
                  if let imageData = dataa {
                      let x = UIImage(data: imageData)
                     cel.titleimg.image = x
                     
                  }
                  else
                  {

                     print(Error.self)
                  }
              }
            cel.titleimg.layer.cornerRadius = cel.titleimg.frame.height/2
            cel.titleimg.clipsToBounds = true
            cel.viewimg.layer.cornerRadius = 4
            cel.viewimg.clipsToBounds = true
            cel.viewimg.layer.borderWidth = 0.2
            cel.viewimg.layer.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            cel.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            cel.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        
            return cel
        }
        else{
            let cel = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! SendtoCollectionViewCell
           
            cel.checkimg.isHidden = true
            collectionViewArray.append(cel)
            cel.imagebacckground.layer.cornerRadius = 4
            cel.imagebacckground.clipsToBounds = true
            cel.imagebacckground.layer.borderWidth = 0.2
            cel.imagebacckground.layer.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            cel.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            return cel
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == self.emergencycollection{
        selectcell = indexPath.item
        print(self.selectcell)
        emergencycollection.reloadData()
        }
        else {
            let cell = collectionViewArray[indexPath.item]
            if cell.checkimg.isHidden == true
            {
                cell.checkimg.isHidden = false
            }
            
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        if collectionView == self.sendtocollection{
        let cell = collectionViewArray[indexPath.item]
        if cell.checkimg.isHidden == false
        {
            cell.checkimg.isHidden = true
        }
        }
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()

        getData()
        sendtocollection.allowsMultipleSelection = true
        emergencyview.layer.borderWidth = 1
        emergencyview.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        senttoview.layer.borderWidth = 1
        senttoview.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        mylocationbtn.layer.cornerRadius = 12
        mylocationbtn.clipsToBounds = true
    }
    
    @IBAction func mylocation(_ sender: UIButton) {
        
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        
        if CLLocationManager.locationServicesEnabled(){
            locationManager.startUpdatingLocation()
        }
        
    }
    func getData(){
           let url = "https://enscyd.com/ngo_api/showReportType.php"
          SVProgressHUD.setDefaultStyle(SVProgressHUDStyle.dark)
                SVProgressHUD.show(withStatus: "Loading")
           var record = [NSDictionary]()
           Alamofire.request(url, method: .post, parameters: nil, encoding: JSONEncoding.default).responseJSON
               {responce in
                print(responce.result)
                   switch responce.result{
                   case .success:
                       if let json = responce.result.value{
                           let JSON = json as! NSDictionary
                           
                           for res in JSON{
                               if ("\(res.key)" == "records")
                               {
                                   record = JSON.object(forKey: res.key as! String) as! [NSDictionary]
                                   for innerResult in record{
                                    
                                       let dicFrmArray = innerResult
                                       
                                       if ((dicFrmArray["id"] as? NSNull) == nil && dicFrmArray["name"] != nil && dicFrmArray["report_image"] != nil ){
                                           
                                        
                                        let datas = Edataa(id: (dicFrmArray["id"]) as? String, name: (dicFrmArray["name"]) as? String, image: (dicFrmArray["report_image"]) as? String)
                                          
                                            self.data.append(datas)
                                        print(datas.name)
                                       }
                                       
                                   }
                                  
                               }
                           }
                           self.emergencycollection.reloadData()
                        SVProgressHUD.dismiss()
                       }
                       else{
                           print("Failure")
                       }
                       break
                   case .failure:
                       if let json = responce.result.value{
                           print(json)
                       }
                       break
                   }
           }
           
       }
    func getgurdinData(){
        let url = "http://enscyd.com/ngo_api/showguradian.php"
       SVProgressHUD.setDefaultStyle(SVProgressHUDStyle.dark)
             SVProgressHUD.show(withStatus: "Loading")
        var record = [NSDictionary]()
        Alamofire.request(url, method: .post, parameters: nil, encoding: JSONEncoding.default).responseJSON
            {responce in
             print(responce.result)
                switch responce.result{
                case .success:
                    if let json = responce.result.value{
                        let JSON = json as! NSDictionary
                        
                        for res in JSON{
                            if ("\(res.key)" == "records")
                            {
                                record = JSON.object(forKey: res.key as! String) as! [NSDictionary]
                                for innerResult in record{
                                 
                                    let dicFrmArray = innerResult
                                    
                                    if ((dicFrmArray["id"] as? NSNull) == nil && dicFrmArray["name"] != nil && dicFrmArray["report_image"] != nil ){
                                        
                                     
                                     let datas = Edataa(id: (dicFrmArray["id"]) as? String, name: (dicFrmArray["name"]) as? String, image: (dicFrmArray["report_image"]) as? String)
                                       
                                         self.data.append(datas)
                                     print(datas.name)
                                    }
                                    
                                }
                               
                            }
                        }
                        self.emergencycollection.reloadData()
                     SVProgressHUD.dismiss()
                    }
                    else{
                        print("Failure")
                    }
                    break
                case .failure:
                    if let json = responce.result.value{
                        print(json)
                    }
                    break
                }
        }
        
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation :CLLocation = locations[0] as CLLocation
        
        print("user latitude = \(userLocation.coordinate.latitude)")
        print("user longitude = \(userLocation.coordinate.longitude)")
        getAddressFromLatLon(pdblLatitude: userLocation.coordinate.latitude, withLongitude : userLocation.coordinate.longitude)
    }
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error \(error)")
    }

    func getAddressFromLatLon(pdblLatitude: Double, withLongitude : Double) {
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        let lat: Double = pdblLatitude
        //21.228124
        let lon: Double = withLongitude
        //72.833770
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = lat
        center.longitude = lon
        
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
        
        
        ceo.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                if (error != nil)
                {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                }
                guard let place = placemarks else
                {
                    print("cannot get Location")
                    return
                    
                }
                let pm = place
                
                if pm.count > 0 {
                    let pm = placemarks![0]
                    //                    print(pm.country)
                    //                    print(pm.locality)
                    //                    print(pm.subLocality)
                    //                    print(pm.thoroughfare)
                    //                    print(pm.postalCode)
                    //                    print(pm.subThoroughfare)
                    var addressString : String = ""
                    if pm.subLocality != nil {
                        addressString = addressString + pm.subLocality! + ","
                    }
                    if pm.thoroughfare != nil {
                        addressString = addressString + pm.thoroughfare! + ","
                    }
                    if pm.locality != nil {
                        addressString = addressString + pm.locality! + ","
                    }
                    if pm.country != nil {
                        addressString = addressString + pm.country! + ", "
                    }
                    if pm.postalCode != nil {
                        addressString = addressString + pm.postalCode! + " "
                    }
                    if addressString != ""{
                        
                        self.locationtext.text = addressString
                    }
                    print(addressString)
                }
        })
        
    }


}

struct Edataa{

    let id : String?

    let name : String?

    let image : String?

}

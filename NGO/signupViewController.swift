

import UIKit

class signupViewController: UIViewController {

    
    @IBOutlet weak var cameraview: UIView!
    @IBOutlet weak var camerabtn: UIButton!
    @IBOutlet weak var addimg: UIImageView!
    @IBOutlet weak var genderview: UIView!
    @IBOutlet weak var btnsignup: UIButton!
    @IBOutlet weak var btnFemale: UIButton!
    @IBOutlet weak var btnMale: UIButton!
   
    override func viewDidLoad() {
        super.viewDidLoad()
        genderview.layer.cornerRadius = 4
        genderview.clipsToBounds = true
        genderview.layer.borderWidth = 0.4
        genderview.layer.borderColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        btnsignup.layer.cornerRadius = 16
        btnsignup.clipsToBounds = true
        cameraview.layer.cornerRadius = cameraview.frame.width/2
        cameraview.clipsToBounds = true
        addimg.layer.cornerRadius = addimg.frame.width/2
        addimg.clipsToBounds = true
        
    }
    
    
    @IBAction func btnClick(_ sender: UIButton) {
        if btnMale.isSelected == true {
            btnMale.isSelected = false;
            btnFemale.isSelected = false
        
    }
       
    }
    

    @IBAction func btnfemale(_ sender: UIButton) {
        if btnFemale.isSelected ==  false {
                btnFemale.isSelected = true;
                btnMale.isSelected = true
            
        }
    }
}

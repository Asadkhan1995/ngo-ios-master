//
//  kiddnapViewController.swift
//  NGO
//
//  Created by Obaid Khan on 04/03/2020.
//  Copyright © 2020 Obaid Khan. All rights reserved.
//

import UIKit
import Alamofire
import  SVProgressHUD

class kiddnapViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet weak var addnewkiddnap: UIButton!
    @IBOutlet weak var tablekiddnap: UITableView!
    var data = [dataa]()
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        let cel = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! kiddnapTableViewCell
        cel.catView.layer.cornerRadius = 3
        cel.catView.clipsToBounds = true
        cel.catView.layer.borderWidth = 0.5
        cel.catView.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        cel.catView.layer.shadowOpacity = 4
        cel.catView.layer.shadowColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        cel.imageview.layer.cornerRadius =  cel.imageview.frame.height / 2
        cel.imageview.clipsToBounds = true
        cel.buttonReportAdmin.layer.cornerRadius = 4
            cel.buttonReportAdmin.layer.borderWidth = 1
        cel.buttonReportAdmin.layer.borderColor = #colorLiteral(red: 0.521568656, green: 0.1098039225, blue: 0.05098039284, alpha: 1)
        cel.name.text = data[indexPath.item].name
        cel.age.text = data[indexPath.item].age
        cel.gender.text = data[indexPath.item].gender
        cel.location.text = data[indexPath.item].address
        cel.discription.text = data[indexPath.item].discription
        cel.phoneNo.text = data[indexPath.item].phone
        let imageName = self.data[indexPath.item].image ?? ""
        
        
        let imageurl = URL(string: "https://enscyd.com/ngo_api/images/"+imageName)!
       if imageurl != nil
         {
            print(imageurl)
            let dataa = try? Data(contentsOf: imageurl)
             if let imageData = dataa {
                 let x = UIImage(data: imageData)
                cel.imageview.image = x
                
             }
             else
             {
                 //self.arrayimg.append(#imageLiteral(resourceName: "User deactivation"))

                print(Error.self)
             }
         }
         
        return cel
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()

        addnewkiddnap.layer.cornerRadius = addnewkiddnap.frame.width/2
        addnewkiddnap.clipsToBounds = true
        getData()
    }
   

    func getData(){
        let url = "https://enscyd.com/ngo_api/showkidnapPeople.php"
       SVProgressHUD.setDefaultStyle(SVProgressHUDStyle.dark)
       SVProgressHUD.show(withStatus: "Loading")
        var record = [NSDictionary]()
        Alamofire.request(url, method: .post, parameters: nil, encoding: JSONEncoding.default).responseJSON
            {responce in
                switch responce.result{
                case .success:
                    if let json = responce.result.value{
                        let JSON = json as! NSDictionary
                        
                        for res in JSON{
                            if ("\(res.key)" == "records")
                            {
                                record = JSON.object(forKey: res.key as! String) as! [NSDictionary]
                                for innerResult in record{
                                 
                                    let dicFrmArray = innerResult
                                    
                                    if ((dicFrmArray["id"] as? NSNull) == nil && dicFrmArray["name"] != nil && dicFrmArray["image"] != nil && dicFrmArray["phone"] != nil && dicFrmArray["address"] != nil && dicFrmArray["created_date"] != nil && dicFrmArray["description"] != nil && dicFrmArray["gender"] != nil && dicFrmArray["age"] != nil ){
                                        
                                     
                                        let datas = dataa(id: (dicFrmArray["id"]) as! String, name: (dicFrmArray["name"]) as! String, image: (dicFrmArray["image"]) as! String, phone: (dicFrmArray["phone"]) as! String, address: (dicFrmArray["address"]) as! String, date: (dicFrmArray["created_date"]) as! String, discription: (dicFrmArray["description"]) as! String, gender: (dicFrmArray["gender"]) as! String, age: (dicFrmArray["age"]) as! String)
                                       
                                         self.data.append(datas)
                                        print(datas.image)
                                       
                                    }
                                    
                                }
                               
                            }
                        }
                        self.tablekiddnap.reloadData()
                         SVProgressHUD.dismiss()
                    }
                    else{
                        print("Failure")
                    }
                    break
                case .failure:
                    if let json = responce.result.value{
                        print(json)
                    }
                    break
                }
        }
        
    }
   

}
struct dataa{
    let id : String?
    let name : String?
    let image : String?
    let phone : String?
    let address : String?
    let date : String?
    let discription : String?
    let gender : String?
    let age : String?

}

//
//  SendtoCollectionViewCell.swift
//  NGO
//
//  Created by Obaid Khan on 02/03/2020.
//  Copyright © 2020 Obaid Khan. All rights reserved.
//

import UIKit

class SendtoCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var titlelbl: UILabel!
    @IBOutlet weak var checkimg: UIImageView!
    @IBOutlet weak var imagesendto: UIImageView!
    @IBOutlet weak var imagebacckground: UIView!
}

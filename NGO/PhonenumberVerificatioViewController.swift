//
//  PhonenumberVerificatioViewController.swift
//  NGO
//
//  Created by Obaid Khan on 25/02/2020.
//  Copyright © 2020 Obaid Khan. All rights reserved.
//

import UIKit
import Firebase
let userDefault = UserDefaults.standard
class PhonenumberVerificatioViewController: UIViewController {

    @IBOutlet weak var phone: UITextField!
  
    @IBOutlet weak var verificationcode: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    @IBAction func verify(_ sender: Any) {
        guard let phoneNum = phone.text else {
            return
        }
        PhoneAuthProvider.provider().verifyPhoneNumber(phoneNum, uiDelegate: nil) { (verificationid, error) in
            if error == nil{
                print(verificationid)
                guard let verifyid = verificationid else {
                    return
                }
                userDefault.setValue(verifyid, forKey: "verificationId")
               userDefault.synchronize()
                let c = self.storyboard?.instantiateViewController(withIdentifier: "OTPViewController") as! OTPViewController
                self.navigationController?.pushViewController(c, animated: true)
            } else {
                print("Unable to get code",error?.localizedDescription)
            }
        }
    }
    
    
}


import UIKit
import Firebase
import SVPinView

class OTPViewController: UIViewController {

     var OPTpin:String?
    @IBOutlet weak var pinview: SVPinView!
    @IBOutlet weak var Resendbutton: UIButton!
    @IBOutlet weak var verifybutton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        pinview.didFinishCallback = { pin in
            self.OPTpin = pin
            
        }
        verifybutton.layer.cornerRadius = 8
        verifybutton.clipsToBounds = true
        
        Resendbutton.layer.cornerRadius = 8
        Resendbutton.layer.borderWidth = 1
        Resendbutton.layer.borderColor = #colorLiteral(red: 0.9552268401, green: 0.2285879882, blue: 0.2530462583, alpha: 1)
        Resendbutton.clipsToBounds = true
    }
    
    
    @IBAction func verifyOPT(_ sender: Any) {
       
            if  let code = self.OPTpin {
                print("The pin entered is \(self.OPTpin ?? "")")
            
        guard let verificationid = userDefault.string(forKey: "verificationId") else {
            return
        }
        let codedential = PhoneAuthProvider.provider().credential(withVerificationID: verificationid, verificationCode: code)
        Auth.auth().signInAndRetrieveData(with: codedential) { (success, error) in
            if error  == nil {
                
                print("success")
                //reedirect from here
                let c = self.storyboard?.instantiateViewController(withIdentifier: "mainpageViewController") as! mainpageViewController
                self.navigationController?.pushViewController(c, animated: true)
            } else {
                print("You Enter wrong code")
            }
            
        
            }
       
        }
        
    }
    
    @IBAction func Resendopt(_ sender: Any) {
        
    }
    
}
